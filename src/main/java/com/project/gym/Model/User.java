package com.project.gym.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity(name = "users")
@Data
@NoArgsConstructor
public class User {

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Id
    @Email
    @NotEmpty
    @Column(unique = true)
    private String email;


    private String name;

    private String surname;

    @Size(min = 4)
    private String password;

    private String phone;

    private String address;

    private String aboniment;

    @Basic
    @Temporal(TemporalType.DATE)
    private Date birthday;

    @Basic
    @Temporal(TemporalType.DATE)
    private Calendar dateAboniment;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name="picture")
    private byte[] picture;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Task> tasks;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES", joinColumns={
            @JoinColumn(name = "USER_EMAIL", referencedColumnName = "email") }, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_NAME", referencedColumnName = "name") })
    private List<Role> roles;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "plan_id", referencedColumnName = "id")
    private Plan plan;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user",fetch = FetchType.EAGER,orphanRemoval = true)
    private List<Weight> weights;

    public User(String email, String name, String password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public User(Long id, @Email @NotEmpty String email, @NotEmpty String name, @Size(min = 4) String password, String phone, String aboniment) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.aboniment = aboniment;
    }

    public User(@Email @NotEmpty String email, @NotEmpty String name, @Size(min = 4) String password, String phone) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.phone = phone;
    }
}
