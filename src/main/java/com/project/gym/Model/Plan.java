package com.project.gym.Model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Plan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(columnDefinition="TEXT", name = "mon")
    private String mon;

    @Column(columnDefinition="TEXT", name = "tue")
    private String tue;

    @Column(columnDefinition="TEXT", name = "wen")
    private String wen;

    @Column(columnDefinition="TEXT", name = "thu")
    private String thu;

    @Column(columnDefinition="TEXT", name = "fri")
    private String fri;

    @Column(columnDefinition="TEXT", name = "sat")
    private String sat;

    @Column(columnDefinition="TEXT", name = "sun")
    private String sun;

    @OneToOne(mappedBy = "plan")
    private User user;


    public Plan(String mon, String tue, String wen, String thu, String fri, String sat, String sun) {
        this.mon = mon;
        this.tue = tue;
        this.wen = wen;
        this.thu = thu;
        this.fri = fri;
        this.sat = sat;
        this.sun = sun;
    }

}
